import random
import time

board_size=3
XO = list("XO")

#Filling board with the '_'
def initiate_board (board):
    
    for i in range (board_size * board_size):
        board.append('_')
    

# Displaying the board in the termial
def print_board (board):
    
    print(f"  {board[0]}  {board[1]}  {board[2]}\n")
  
    print(f"  {board[3]}  {board[4]}  {board[5]}\n")
   
    print(f"  {board[6]}  {board[7]}  {board[8]}\n")



# Checking if there is a winner - either one column, row or diagonal
# is filled with the char
def winner_check (char, board):
    

    #check rows
    if ((board [0] == char)and (board [1] == char)and (board [2] == char)) or ((board [3] == char)and (board [4] == char)and (board [5] == char)) or((board [3] == char)and (board [4] == char)and (board [5] == char)):
        return True
    
    


    #check columns
    if ((board [0] == char)and (board [3] == char)and (board [6] == char)) or ((board [1] == char)and (board [4] == char)and (board [7] == char)) or ((board [2] == char)and (board [5] == char)and (board [8] == char)):
        return True
    

    #check diagonals
    if ((board [0] == char)and (board [4] == char)and (board [8] == char)) or ((board [2] == char)and (board [4] == char)and (board [6] == char)) :
        
        return True

    return False
    

    
# Checking the current status of the board
#
# Results
#    "D" - Draw
#    "X" - Victory of the X player
#    "Y" - Victory of the Y player
#    "G" - Game not finished yet
#
def check_result (board):
    if len(available_moves) == 0:
        return "D" #draw
    elif winner_check ('X', board):
        return 'X' # X wins
    elif winner_check ('O', board):
        return 'O' #O wins
    return 'G' #Game continues


# Collect computer move and update available_moves and display_moves tables
def computer_move(char):

    move = random.choice(available_moves)


    main_board[int(move)-1] = char

    # Removing move from the available_moves
    for i in range(len(available_moves)): 
        if available_moves[i] == move:
            available_moves.pop(i)
            break
    # Removing move from the display_moves    
    for i in range(len(display_moves)): 
        if display_moves[i] == str(move):
            display_moves[i]='_'
            break

         

    print(f" COMPUTER PLAYS {char} ON THE {move} FIELD")

    print_board(main_board)
    time.sleep(1)

    
# Collect human move and update available_moves and display_moves tables
def human_move(char):

    print(f"WHERE WOULD YOU LIKE TO PUT {char}? \n")
    print_board(display_moves)
    move_collected = False
    while not move_collected:
    
        
        move = input()
        if move in display_moves and move != '_':
            move_collected = True
        else:
            print("USE ONLY VALID MOVES")
    
    

    main_board[int(move)-1] = char

    # Removing move from the available_moves
    for i in range(len(available_moves)): 
        if available_moves[i] == int(move):
            available_moves.pop(i)
            break
    # Removing move from the display_moves    
    for i in range(len(display_moves)): 
        if display_moves[i] == move:
            display_moves[i]='_'
            break

         

    print(f" HUMAN PLAYS {char} ON THE {move} FIELD")

    print_board(main_board)




print("*****     *****     *****     *****     *****     *****\n")  
print("**                                                   **\n")

print("**                PYTHONIC TIC TAC TOE               **\n")

print("**                                                   **\n")
print("*****     *****     *****     *****     *****     *****\n")

print("Connecting to NORAD....")
      
time.sleep(2)

print ("Connection established")
time.sleep(1)
      
print("\n\n GREETINGS PROFESSOR FALKEN\n")
print("\n\n SHALL WE PLAY A GAME?\n")

game_in_progress=True


# Main program loop
while game_in_progress:

    # program global variables initiation
    
    # main_board - current status of the game [String]
    main_board = []
    initiate_board(main_board)

    # available_moves - list of moves available int he game. [Int]
    available_moves = [1, 2, 3, 4 ,5 ,6 , 7, 8, 9]

    #display_moves - list with strings to display in the terminal [String]
    display_moves = list("123456789")


    #Collecting numer of human players
    players_num_collected=False 
    while not players_num_collected:
        human_players_num=input("\n HOW MANY HUMAN PLAYERS WILL PLAY? [0/1/2] ")
        players_num_collected = human_players_num in ('0', '1', '2')

    print(f"\n GREAT!\n")
    if human_players_num =='0':
        player1=False
        player2=False
    elif human_players_num =='1':
        player1=True
        player2=False
    else:
        player1=True
        player2=True

    
    if player1:
        print(f" PLAYER1 - HUMAN 'X' \n")
    else:
        print(f" PLAYER1 - COMPUTER 'X'\n")

   
    if player2:
        print(f" PLAYER2 - HUMAN 'O'\n")
    else:
        print(f" PLAYER2 - COMPUTER 'O'\n")    

     
    # determines which player starts the game
    first_player = random.randint(1,2)
    if first_player == 1:
        second_player = 2
    else:
        second_player = 1
    print(f"PLAYER{first_player} WILL MOVE FIRST \n")
        
    # sequence[] is a list of 2 Boolean values representing players and their sequence of moves
    #  HUMAN - True
    #  COMPUTER - FALSE
    #
    sequence = []
    if first_player == 1:
        sequence.extend([player1, player2])
    else:
        sequence.extend([player2, player1])

    # loop of the actual game moves
    seq_count=0
    available_moves_exist=True

    while available_moves_exist:
        

    
        if sequence[seq_count]:
            human_move(XO[seq_count])
        else:
            computer_move(XO[seq_count])
        seq_count = abs(seq_count - 1)

        game_status=check_result(main_board)

        if game_status== "D":
            available_moves_exist = False
            print("DRAW")
        elif game_status == "X":
            available_moves_exist = False
            print("X WON")
        elif game_status == "O":
            available_moves_exist = False
            print("O WON")   

    # Checking if player would like to play another game         
    res_cont = False
    while not res_cont:
        q_continue = input("WOULD YOU LIKE TO PLAY ANOTHER GAME? (Y/N) ")
        if q_continue.upper()  in ('N', 'Y'):
            res_cont = True

        
    if q_continue.upper() == 'N':
        game_in_progress=False    






print("THANK YOU FOR YOUR TIME")
